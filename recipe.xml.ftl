<?xml version="1.0"?>
<recipe>

	<instantiate from="src/app_package/Fragment.java.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${name}Fragment.java" />
	<instantiate from="src/app_package/MvpView.java.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${name}MvpView.java" />
	<instantiate from="src/app_package/Presenter.java.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${name}Presenter.java" />
   <instantiate from="src/app_package/View.xml.ftl"
                   to="${escapeXmlAttribute(resOut)}/layout/${layoutName}.xml" />


	<open file="${srcOut}/${name}Fragment.java"/>
</recipe>