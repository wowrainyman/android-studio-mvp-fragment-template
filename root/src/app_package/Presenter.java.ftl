package ${packageName};

<#if applicationPackage??>
import ${applicationPackage}.data.DataManager;
import ${applicationPackage}.features.base.BasePresenter;
</#if>


import javax.inject.Inject;

public class ${name}Presenter extends BasePresenter<${name}MvpView> {
    private final DataManager dataManager;

    @Inject
    public ${name}Presenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void attachView(${name}MvpView mvpView) {
        super.attachView(mvpView);
    }

}