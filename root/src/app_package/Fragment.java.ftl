package ${packageName};

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
<#if applicationPackage??>
import ${applicationPackage}.R;
import ${applicationPackage}.features.base.BaseFragment;
import ${applicationPackage}.injection.component.FragmentComponent;
</#if>


import javax.inject.Inject;

public class ${name}Fragment extends BaseFragment implements ${name}MvpView {

    
    @Inject ${name}Presenter m${name}Presenter;

    @Override
    protected int getLayout() {
        return R.layout.fragment_${classToResource(name)};
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        m${name}Presenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        m${name}Presenter.detachView();
    }

    public ${name}Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(getLayout(), container, false);
        return view;
    }



}